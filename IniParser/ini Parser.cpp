// ini Parser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <unordered_map>
using namespace std;


class IniParser {
private:
	string inifilename;
	unordered_map<string, unordered_map<string, string>> map;
public:
	IniParser() {

	}
	IniParser(string filename) {
		Parse(filename);
	}
	void Parse(string filename) {
		inifilename = filename;
		ifstream file(filename);

		string str;
		string section;

		unordered_map<string, string> tmp;
		map.clear();

		while (getline(file, str)) {
			//empty string
			if (str.empty()) {
				continue;
			} 
			else 
			//section
			if (str.at(0) == '[' && (str.at(str.length() - 1) == ']')) {
				if (tmp.size() > 0) {
					map.insert(map.end(), pair<string, unordered_map<string, string>>(section, tmp));
				}
				tmp.clear();
				section = str.substr(1, str.length() - 2);
			}
			else
			//key-value 
			{
				string key = str.substr(0, str.find('='));
				string value = str.substr(str.find('=') + 1, str.length() - str.find('='));
				tmp.insert(tmp.end(), pair<string, string>(key, value));
			}
		}

		if (tmp.size() > 0) {
			map.insert(map.end(), pair<string, unordered_map<string, string>>(section, tmp));
		}

		file.close();
	
	};
	string GetValue(string section, string key) {
		unordered_map<string, unordered_map<string, string>>::const_iterator it = map.find(section);
		if (it == map.end()) {
			return "Section not found";
		} else {
			unordered_map<string, string> sectionMap = it->second;
			unordered_map<string, string>::const_iterator sit = sectionMap.find(key);
			if (sit == sectionMap.end()) {
				return "Key not found";
			}
			else {
				return sit->second;
			}
		}
	};
	string GetFilename() {
		return inifilename;
	}
	unordered_map<string, unordered_map<string, string>> GetAll() {
		return map;
	}

};


int main()
{
	IniParser parser("C:\\..\\...\\...\\...\\C++\\Ini Parser\\...\\...\\info.ini");
	string result = parser.GetValue("VIDEO", "resolutionX");
	cout << result;
	getchar();
	
}

