// smartPointer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

template <typename T> class SmartPointer {
private:
	T* pointer = NULL;
	int* counter = NULL;

	void FreePointer() {
		*counter = *counter - 1;
		if (counter == 0) {
			delete pointer;
			free(counter);
			pointer = NULL;
			counter = NULL;
		}
	}
public:

	SmartPointer(T* obj) {
		pointer = obj;
		counter = (int*)malloc(sizeof(int));
		*counter = 1;
	};

	SmartPointer(SmartPointer<T>& obj) {
		pointer = obj.pointer;
		counter = obj.counter;
		*counter = *counter + 1;
	}

	T* operator-> () {
		return pointer;
	};

	T& operator* () {
		return *pointer;
	}

	int NumberOfLinks() {
		return *counter;
	}

	SmartPointer& operator = (SmartPointer& obj) {
		if (this != obj) {
			FreePointer();
			pointer = obj.pointer;
			counter = obj.counter;
			*counter = *counter + 1;
		}
		return *this;
	}

	~SmartPointer() {
		FreePointer();
	};


};

class Test {
public:
	int b = 0;

	Test() {
		b = 1;
	}

	~Test() {
		b = 0;
	}
};


int main()
{
	std::cout << "Create a Test class \n";
	Test* t = new Test();
	std::cout << "Number in test class: " << t->b << "\n";
	std::cout << "*************** \n";
	std::cout << "Create smart pointer p1 \n";
	SmartPointer<Test> p1(t);
	std::cout << "Test class value from smart pointer p1: " << p1->b << "\n";
	std::cout << "Number of links p1: " <<  p1.NumberOfLinks() << "\n";
	std::cout << "**************** \n";
	std::cout << "Create smart pointer p2 \n";
	std::cout << "Create smart pointer p3 \n";
	SmartPointer<Test> p2(p1);
	SmartPointer<Test> p3 = p2;
	std::cout << "Test class value from smart pointer p1: " << p1->b << "\n";
	std::cout << "Number of links p1: " << p1.NumberOfLinks() << "\n";
	std::cout << "Test class value from smart pointer p2: " << p2->b << "\n";
	std::cout << "Number of links p2: " << p2.NumberOfLinks() << "\n";
	std::cout << "Test class value from smart pointer p3: " << p2->b << "\n";
	std::cout << "Number of links p3: " << p2.NumberOfLinks() << "\n";
	std::cout << "**************** \n";
	std::cout << "Delete smart pointer p1 \n";
	p1.~SmartPointer();
	std::cout << "Test class value from smart pointer p2: " << p2->b << "\n";
	std::cout << "Number of links p2: " << p2.NumberOfLinks() << "\n";
	std::cout << "Test class value from smart pointer p3: " << p3->b << "\n";
	std::cout << "Number of links p3: " << p3.NumberOfLinks() << "\n";
	std::cout << "**************** \n";
	std::cout << "Delete smart pointer p2 \n";
	p2.~SmartPointer();
	std::cout << "Test class value from smart pointer p3: " << p3->b << "\n";
	std::cout << "Number of links p3: " << p3.NumberOfLinks() << "\n";
	Test t2 = *p3;
	std::cout << "Value from test class t2 = *p3 : " << t2.b;
	getchar();
}

